import React,{Component} from 'react'
// import $ from 'jquery'; 
import { BrowserRouter,Switch,Route } from 'react-router-dom'
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard' 
import ProjectDetails from './components/project/Projectdetails';
import Signin from './components/auth/Signin';
import Signup from './components/auth/Signup';
import Createproject from './components/project/Createproject';




class App extends Component {

  render (){

    return (
        <BrowserRouter>
          
             <div className="App">
               <Navbar />
                <Switch>
                  <Route exact path="/" component={Dashboard} />
                  <Route path="/project/:id" component={ProjectDetails}></Route>
                  <Route path="/signin" component={Signin}></Route>
                  <Route path="/signup" component={Signup}></Route>
                  <Route path="/createproject" component={Createproject} />
                </Switch>
                      
            </div>
                  
        </BrowserRouter>
    );
  }
  
}

export default App;
