import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/jquery.js';
import 'popper.js/dist/popper.js';
import 'bootstrap/dist/js/bootstrap.js';
import "materialize-css/dist/css/materialize.css"
import "materialize-css/dist/js/materialize"
import rootReducer from './store/reducers/rootReducer'
import App from './App';
import reportWebVitals from './reportWebVitals';
import {createStore,applyMiddleware,compose} from 'redux';
import {Provider,useSelector } from 'react-redux';
import thunk from 'redux-thunk'
import {reduxFirestore,createFirestoreInstance,getFirestore} from 'redux-firestore'
import {ReactReduxFirebaseProvider,getFirebase, isLoaded } from 'react-redux-firebase'
import firebaseConfig from './config/fbConfig'
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const rrfConfig = { 
  userProfile: "users",
  useFirestoreForProfile: true,
  attachAuthIsReady:true
};


const store =  createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk.withExtraArgument({getFirebase,getFirestore})),
    reduxFirestore(firebaseConfig)
    )
);



const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance, 
    
};

function AuthIsReady({ children }){
  const auth = useSelector(state=>state.firebase.auth);
  if(isLoaded(auth)){
    return children;
  }
  else{
    return <div className="center loading"> <p className="pt-5 display-1 text-primary">Loading</p> </div>
  }
}

  // store.firebaseAuthIsReady.then(()=>{
    ReactDOM.render(
      <Provider store={store}>
        <ReactReduxFirebaseProvider {...rrfProps}>
          <AuthIsReady>    
            <App />
          </AuthIsReady>
        </ReactReduxFirebaseProvider>
      </Provider>,
      document.getElementById('root')
    );

    reportWebVitals();
  // })


