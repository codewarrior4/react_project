import React, { Component } from 'react';
import Notification from './Notification'
import Projectlist from '../project/Projectlist'
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import {Redirect} from 'react-router-dom'
class Dashboard extends Component{
    render()
    {
       const {projects,auth} = this.props
       if(!auth.uid) return <Redirect to='/signin' />
        return(
            
            <div className="dashboard container">
                <div className="row">

                </div>
                <Row>
                    <Col>
                        <h1 className="display-4">Project List</h1> <br/> <hr/>
                        <Projectlist projects={projects} /> 
                    </Col>
                    <Col>
                        <h1 className="display-4">Notification</h1> <br/> <hr/>
                        <Notification />
                    </Col>
                </Row>
                    

            </div>
        )
    }
}

const mapStateToProps =(state) =>{
    return {
        projects:state.firestore.ordered.projects,
        auth:state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([{collection:'projects'}]),
)(Dashboard) 