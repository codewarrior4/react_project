import React, { Component } from 'react'
import {connect} from 'react-redux'
import {SignIn} from '../../store/actions/authAction'
import {Redirect} from 'react-router-dom'

 class Signin extends Component {
     state={
         email:"",
         password:""
     }
     handleChange=(e)=>{
         this.setState({
             [e.target.id]:e.target.value
             
         },()=>{
            //  console.log(this.state)
         })
         
     }
    handleSubmit=(e)=>{
        e.preventDefault()
        this.props.SignIn(this.state)

        this.setState({
            email:"",
            password:""
        })
    }
   
    render() {
        const {authError,auth}=this.props
        if(auth.uid) return <Redirect to='/'/>
        return (
            <div className="container ">
                <form onSubmit={this.handleSubmit} className="white">
                    <h4 className="blue-text text-darken-4">Sign In</h4>
                    <div className="input-field">
                        <label htmlFor="email">Email </label>
                        <input value={this.state.email} type="email" id="email" onChange={this.handleChange} />
                    </div>
                    <div className="input-fiel">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" value={this.state.password} onChange={this.handleChange} />

                    <div className="input-field">
                        <button className="btn blue lighten-1 z-depth-0">Sign in</button>
                    </div>
                    <div>
                       
                            {authError ? 
                            <div className="alert alert-info alert-dismissible " role="alert">
                                
                                <p className="center font-weight-bold">
                                {authError}
                                </p>
                            </div>
                             :null}
                        
                    </div>
                    </div>
                </form>
            </div>
        )
    }
}
const mapStateToProps=(state) =>{
    return {
        authError:state.auth.authError,
        auth:state.firebase.auth
    }
}

const mapDispatchToProps =(dispatch)=>{
    return{
        SignIn:(creds)=>dispatch(SignIn(creds))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Signin)
