import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import {signUp} from '../../store/actions/authAction'
const styles ={
    boxShadow:"15px 5px 3px grey,-15px -5px 3px grey,-15px -5px 3px grey",
    padding:"6px 10px",
    margin:"40px auto"
}
 class Signup extends Component {
     state={
         firstName:"",
         lastName:"",
         email:"",
         password:""
     }
     handleChange=(e)=>{
         this.setState({
             [e.target.id]:e.target.value
             
         })
         
     }
    handleSubmit=(e)=>{
        e.preventDefault()
        // console.log(this.state)
        this.props.signUp(this.state)
    }
    
    render() {
        const {authError,auth}=this.props
        if(auth.uid) return <Redirect to='/'/>
        return (
            <div className="container ">
                <form style={styles} onSubmit={this.handleSubmit} className="white">
                    <h4 className="blue-text text-darken-4">Sign up</h4>
                    <div className="input-field">
                        <label htmlFor="firstname">Firstname</label>
                        <input type="text" id="firstName" onChange={this.handleChange} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastname">Lastname</label>
                        <input type="text" id="lastName" onChange={this.handleChange} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type="email" id="email" onChange={this.handleChange} />
                    </div>
                    <div className="input-fiel">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" onChange={this.handleChange} />

                    <div className="input-field">
                        <button className="btn blue lighten-1 z-depth-0">Sign up</button>
                    </div>
                    {authError ? 
                            <div className="alert alert-info alert-dismissible " role="alert">
                                
                                <p className="center font-weight-bold">
                                {authError}
                                </p>
                            </div>
                             :null}
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps =(state)=>{
    
    return{
        auth:state.firebase.auth,
        authError:state.auth.authError
    }
}

const mapDispatchToProps =(dispatch)=>{
    return{
        signUp:(creds)=>dispatch(signUp(creds))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Signup)