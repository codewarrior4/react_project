import React from 'react';
import Projectsummary from './Projectsummary';
import {Link} from 'react-router-dom'

const Projectlist= ({projects}) =>{

    return (
        <div className="project-list">
            <div className="card border border-0 z-depth-0 project-summary">
                
                {projects && projects.map(project=>{
                    
                    return (
                        <Link to={"/project/"+project.id} key={project.id}>
                        <Projectsummary project={project}/>
                        </Link>
                    )

                })}
             </div>
        </div>
    )
}

export default Projectlist