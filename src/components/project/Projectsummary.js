import React from 'react';
import moment from 'moment'

const Projectsummary =({project})=>{
    return (
        <div style={{borderRadius:"4px",marginTop:"4px",border:"1px solid blue"}}  className="m-3 card-content blue-text text-darken-3">
            <span className="card-title"> {project.title}</span>
            <p>Posted by {project.authorFirst +" "+ project.authorLast}</p>
            <p className="blue-text">{moment(project.createdAt.toDate()).calendar()}</p>
        </div>
        
    )
}



export default Projectsummary