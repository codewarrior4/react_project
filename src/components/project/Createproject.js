import React, { Component } from 'react'
import {connect} from 'react-redux'
import {createProject} from '../../store/actions/projectActions'
import {Redirect} from 'react-router-dom'

class Createproject extends Component {
    state={
        title:"",
        content:""
    }

    handleChange=(e)=>{
        this.setState({
            [e.target.id]:e.target.value
            
        })
        
    }

   handleSubmit=(e)=>{
       e.preventDefault()
       this.props.createProject(this.state)
       this.props.history.push('/')
    //    console.log(this.state)
    this.setState({
        title:"",
        content:""
    })
        
   }
    render() {   
        console.log(this.state)
       const {auth}=this.props
       if(!auth.uid) return <Redirect to='/signin' />
           return (
               <div className="container ">
                   <form onSubmit={this.handleSubmit} className="white">
                       <h4 className="blue-text text-darken-4">Add New Project</h4>
                       <div className="input-field">
                           <label htmlFor="title">Title</label>
                           <input type="text" value={this.state.title} id="title" onChange={this.handleChange} />
                       </div>
                       <div className="input-field">
                           <label htmlFor="content">Content</label>
                           <textarea cols="30" rows="10"
                           className="materialize-textarea"
                           style={{resize:"none",border:"none",outline:"none",borderBottom:"1px solid gray"}} onChange={this.handleChange} id="content" value={this.state.content}>
                               
                           </textarea>
                       </div>
                       <div className="input-field">
                           <button className="btn blue lighten-1 z-depth-0">Add Project   </button>
                       </div>
                       
                   </form>
               </div>
           )
   
    }
}

const mapStateToProps= (state)=>{
    return{
        ...state,
        auth:state.firebase.auth
    }
}

const mapDispatchToProps =(dispatch)=>{
    return{
     createProject:(project)=>dispatch(createProject(project))    
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Createproject)