import React from 'react';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Signedin from './Signedin' 
import Signedout from './Signedout'
import {connect} from 'react-redux'

const Navbar = (props)=>{
    const{auth,profile}=props
    // console.log(profile)

    const links =auth.uid ? <Signedin profile={profile}/>: <Signedout />
    return (
       
            <nav className="navbar navbar-expand-lg sticky-top navbar-light bg-light">
                <Container>
                <Link className="navbar-brand blue-text " to="/">Codewarrior</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    {links}
                </Container>  
            </nav>
        
    )
}

const mapStateToProps =(state)=>{
    console.log(state)
    return{
        profile:state.firebase.profile,
        auth:state.firebase.auth
    }
}

export default connect(mapStateToProps)(Navbar)