import React from 'react';
import { NavLink } from 'react-router-dom';


const Signedout= ()=>{

    return(
        <ul className="navbar-nav">
            <li className="nav-item "><NavLink className="nav-link" to="/signup">Signup</NavLink></li>
            <li className="nav-item "><NavLink className="nav-link" to="/signin">Log in</NavLink></li>
          
        </ul>
    )
}

export default Signedout 