import React from 'react';
import { NavLink } from 'react-router-dom';
import {connect} from 'react-redux'
import {signOut} from '../../store/actions/authAction'

const Signedin = (props) => {

    return(
        <ul className="navbar-nav ">
            <li className="nav-item "><NavLink className="nav-link" to="/createproject">New Project</NavLink></li>
            <li className="nav-item "><a className="nav-link" onClick={props.signOut} >Log out</a></li>
            <li className="nav-item "><NavLink className="nav-link" to="/" className="btn btn-floating red lighten-2 text-gray">{props.profile.initals}</NavLink></li>
        </ul>
    )
}


// const mapStateToProps =(state)=>{
//     return
// }

const mapDispatchToProps=(dispatch)=>{
    return {
        signOut:()=>dispatch(signOut())
    }
}

export default connect(null,mapDispatchToProps)(Signedin)