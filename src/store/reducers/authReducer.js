const initState = {
    authError:null
}
const authReducer=(state=initState,action)=>{

    switch(action.type){
        case "LOGIN_ERROR":
            console.log('login error')
            return {
                ...state,
                authError:"Login Failed"+action.err 
            }
       
        case "LOGIN_SUCCESS": 
            console.log(" LOGIN_SUCCESS")  
            return  {
                ...state,
                authError:"Login Successful"
            }  

        case "SIGNOUT_SUCCESS":
            console.log("signout success")
            return state

        case "SIGNUP_SUCCESS":
            console.log(state)
            return{
                ...state,
                authError:null
            }

        case "SIGNUP_ERROR":
            console.log(state)
            return{
                ...state,
                authError:"Registration Failed"+ action.err.message             
            }
            
        default:
            return state;
    }
    
}


export default authReducer